<?php
/**
 * Elgg pageshell
 * The standard HTML page shell that everything else fits into
 *
 * @package Elgg
 * @subpackage Core
 *
 * @uses $vars['head']        Parameters for the <head> element
 * @uses $vars['body_attrs']  Attributes of the <body> tag
 * @uses $vars['body']        The main content of the page
 * @uses $vars['sysmessages'] A 2d array of various message registers, passed from system_messages()
 */

// render content before head so that JavaScript and CSS can be loaded. See #4032
$content = elgg_view('page/elements/body', $vars);

$default_items = elgg_extract('default', $vars['menu'], array());
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    
<?php
$site = elgg_get_site_entity();
$site_url = elgg_get_site_url();
$site_name = $site->name;
echo elgg_view('page/elements/head', $vars['head']);
?>
   
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="<?php echo $site_url; ?>" class="site_title">
                  <i class="fa fa-paw"></i> 
                  <span><?php echo $site_name; ?></span>
              </a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
           
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <?php 
                      
                      echo elgg_view('gentelella_theme/layouts/gentelella_sidebar');
            ?>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
                </br>
              </br>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <?php 
                      
                      echo elgg_view('gentelella_theme/layouts/gentelella_top_navigation');
        ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <!-- top tiles -->
          <div class="row tile_count">
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
               
            </div>
            
              
          </div>
          <!-- /top tiles -->

          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="dashboard_graph">

                <div class="row x_title">
                  <div class="col-md-12">
                    <h3> <small> </small></h3>
                  </div>
                  <div class="col-md-12">
                    <?php echo $content ?>
                      
                  </div>
                </div>

               
                

                <div class="clearfix"></div>
              </div>
            </div>

          </div>
          <br />

          <div class="row">


            

          </div>


          
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <?php 
        
        echo elgg_view('gentelella_theme/layouts/gentelella_footer');
        
        ?>
        <!-- /footer content -->
        <?php 
        
        echo elgg_view('page/elements/foot');
        
        ?>
        
      </div>
    </div>

   
	
  </body>
</html>
